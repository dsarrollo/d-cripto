import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:graficas/models/l2-model.dart';
import 'package:graficas/models/prices-model.dart';
import 'package:graficas/models/ticker.model.dart';
import 'package:graficas/models/trades-model.dart';
import 'package:graficas/services/variables-services.dart';
import 'package:graficas/widgets/chart-widget.dart';

/* enum ServerStatus {
  Offline,
  Online,
  Connecting,
} */

class SocketService with ChangeNotifier {
  WebSocket dataSocket;

  void close() {
    dataSocket?.close();
  }

  SocketService() {
    initConnectWebsocket();
  }

  initConnectWebsocket() async {
    this.dataSocket = await connectedWs();
    globals.serverStatus = ServerStatus.Online;
    notifyListeners();
    this.dataSocket.done.then((dynamic _) => print('[+]Done :)'));
    subscribeTicker();
    subscribePrices();
    subscribeL2();
    subscribeTrades();
    listenWebsocket();
  }

  connectedWs() async {
    try {
      return await WebSocket.connect(
          'wss://ws.prod.blockchain.info/mercury-gateway/v1/ws',
          headers: {'origin': 'https://exchange.blockchain.com'});
    } catch (e) {
      globals.serverStatus = ServerStatus.Connecting;
      print('Error! can not connect WS connectedWs ' + e.toString());
      await Future.delayed(Duration(milliseconds: 10000));
      return await connectedWs();
    }
  }

  subscribeTicker() {
    print(globals.dropdownValue);
    this.dataSocket.add(json.encode({
          'action': 'subscribe',
          'channel': 'ticker',
          'symbol': globals.dropdownValue,
        }));
  }

  unsubscribeTicker() {
    this.dataSocket.add(json.encode({
          'action': 'unsubscribe',
          'channel': 'ticker',
          'symbol': globals.dropdownValue,
        }));
  }

  subscribePrices() {
    this.dataSocket.add(json.encode({
          'action': 'subscribe',
          'channel': 'prices',
          'symbol': globals.dropdownValue,
          'granularity': 300
        }));
  }

  unsubscribePrices() {
    this.dataSocket.add(json.encode({
          'action': 'unsubscribe',
          'channel': 'prices',
          'symbol': globals.dropdownValue,
        }));
  }

  subscribeL2() {
    this.dataSocket.add(json.encode({
          'action': 'subscribe',
          'channel': 'l2',
          'symbol': globals.dropdownValue
        }));
  }

  unsubscribeL2() {
    Map<String, String> dataSend = {
      'action': 'unsubscribe',
      'channel': 'l2',
      'symbol': globals.dropdownValue
    };
    this.dataSocket.add(json.encode(dataSend));
  }

  subscribeL3() {
    this.dataSocket.add(json.encode({
          'action': 'subscribe',
          'channel': 'l3',
          'symbol': globals.dropdownValue,
        }));
  }

  unsubscribeL3() {
    this.dataSocket.add(json.encode({
          'action': 'unsubscribe',
          'channel': 'l3',
          'symbol': globals.dropdownValue,
        }));
  }

  subscribeTrades() {
    this.dataSocket.add(json.encode({
          'action': 'subscribe',
          'channel': 'trades',
          'symbol': globals.dropdownValue,
        }));
  }

  unsubscribeTrades() {
    this.dataSocket.add(json.encode({
          'action': 'unsubscribe',
          'channel': 'trades',
          'symbol': globals.dropdownValue,
        }));
  }

  //websocket
  listenWebsocket() {
    this.dataSocket.listen((data) {
      // print(data);
      final jsonResponse = json.decode(data);
      if (jsonResponse['channel'] == 'prices') {
        if (jsonResponse['price'] != null && globals.graphData.length > 0) {
          final dataFinsh = priceWebsocketModelFromJson(data.toString());
          // print(dataFinsh.price);
          DateTime datePrice =
              DateTime.fromMicrosecondsSinceEpoch(dataFinsh.price[0].toInt());
          // String formattedTime = DateFormat.Hm().format(datePrice);
          globals.graphData.add(MyRow(datePrice, dataFinsh.price[2]));
        }
        notifyListeners();
      } else if (jsonResponse['channel'] == 'ticker') {
        final resp = tickerModelFromJson(data.toString());
        if (resp.price24H > 0) {
          globals.price24H = resp.price24H;
        } else {
          resp.lastTradePrice = globals.price24H;
        }
        if (resp.lastTradePrice > 0) {
          globals.lastTradePrice = resp.lastTradePrice;
        } else {
          resp.lastTradePrice = globals.lastTradePrice;
        }
        globals.ticker = resp;
        notifyListeners();
      } else if (jsonResponse['channel'] == 'l2' &&
          jsonResponse['bids'] != null) {
        //print("---------- L ${jsonResponse['symbol']}");
        if (jsonResponse['bids'].length > 0) {
          final resp =
              l2ModelFromJson(json.encode(jsonResponse['bids'][0]).toString());
          if (resp.qty > 0) {
            globals.l2Bids[globals.l2BidsCount] = resp;
            globals.l2BidsCount++;
            globals.l2BidsCount =
                (globals.l2BidsCount >= 8) ? 0 : globals.l2BidsCount;
            notifyListeners();
          }
        } else if (jsonResponse['asks'].length > 0) {
          final resp =
              l2ModelFromJson(json.encode(jsonResponse['asks'][0]).toString());
          if (resp.qty > 0) {
            globals.l2Asks[globals.l2AsksCount] = resp;
            globals.l2AsksCount++;
            globals.l2AsksCount =
                (globals.l2AsksCount >= 8) ? 0 : globals.l2AsksCount;
            notifyListeners();
          }
        }
      } else if (jsonResponse['channel'] == 'trades' &&
          jsonResponse['price'] != null) {
        final resp = tradesModelFromJson(data.toString());
        globals.trades[globals.tradesCount] = resp;
        globals.tradesCount++;
        globals.tradesCount =
            (globals.tradesCount >= 16) ? 0 : globals.tradesCount;
        notifyListeners();
      }
    }, onDone: () {
      print('[+]Done :)');
      initConnectWebsocket();
    }, onError: (e) {
      print('Server error: $e');
      initConnectWebsocket();
    });
  }
}
