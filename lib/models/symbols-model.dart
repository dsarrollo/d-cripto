// To parse this JSON data, do
//
//     final symbolsModel = symbolsModelFromJson(jsonString);

import 'dart:convert';

Map<String, SymbolsModel> symbolsModelFromJson(String str) =>
    Map.from(json.decode(str)).map(
        (k, v) => MapEntry<String, SymbolsModel>(k, SymbolsModel.fromJson(v)));

String symbolsModelToJson(Map<String, SymbolsModel> data) => json.encode(
    Map.from(data).map((k, v) => MapEntry<String, dynamic>(k, v.toJson())));

class SymbolsModel {
  SymbolsModel({
    this.auctionPrice,
    this.auctionSize,
    this.auctionTime,
    this.baseCurrency,
    this.baseCurrencyScale,
    this.counterCurrency,
    this.counterCurrencyScale,
    this.id,
    this.imbalance,
    this.lotSize,
    this.lotSizeScale,
    this.maxOrderSize,
    this.maxOrderSizeScale,
    this.minOrderSize,
    this.minOrderSizeScale,
    this.minPriceIncrement,
    this.minPriceIncrementScale,
    this.status,
  });

  double auctionPrice;
  double auctionSize;
  String auctionTime;
  String baseCurrency;
  int baseCurrencyScale;
  String counterCurrency;
  int counterCurrencyScale;
  int id;
  double imbalance;
  int lotSize;
  int lotSizeScale;
  int maxOrderSize;
  int maxOrderSizeScale;
  int minOrderSize;
  int minOrderSizeScale;
  int minPriceIncrement;
  int minPriceIncrementScale;
  Status status;

  factory SymbolsModel.fromJson(Map<String, dynamic> json) => SymbolsModel(
        auctionPrice: json["auction_price"],
        auctionSize: json["auction_size"],
        auctionTime: json["auction_time"],
        baseCurrency: json["base_currency"],
        baseCurrencyScale: json["base_currency_scale"],
        counterCurrency: json["counter_currency"],
        counterCurrencyScale: json["counter_currency_scale"],
        id: json["id"],
        imbalance: json["imbalance"],
        lotSize: json["lot_size"],
        lotSizeScale: json["lot_size_scale"],
        maxOrderSize: json["max_order_size"],
        maxOrderSizeScale: json["max_order_size_scale"],
        minOrderSize: json["min_order_size"],
        minOrderSizeScale: json["min_order_size_scale"],
        minPriceIncrement: json["min_price_increment"],
        minPriceIncrementScale: json["min_price_increment_scale"],
        status: statusValues.map[json["status"]],
      );

  Map<String, dynamic> toJson() => {
        "auction_price": auctionPrice,
        "auction_size": auctionSize,
        "auction_time": auctionTime,
        "base_currency": baseCurrency,
        "base_currency_scale": baseCurrencyScale,
        "counter_currency": counterCurrency,
        "counter_currency_scale": counterCurrencyScale,
        "id": id,
        "imbalance": imbalance,
        "lot_size": lotSize,
        "lot_size_scale": lotSizeScale,
        "max_order_size": maxOrderSize,
        "max_order_size_scale": maxOrderSizeScale,
        "min_order_size": minOrderSize,
        "min_order_size_scale": minOrderSizeScale,
        "min_price_increment": minPriceIncrement,
        "min_price_increment_scale": minPriceIncrementScale,
        "status": statusValues.reverse[status],
      };
}

enum Status { OPEN }

final statusValues = EnumValues({"open": Status.OPEN});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
