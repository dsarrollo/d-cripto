import 'package:flutter/material.dart';
import 'package:graficas/models/l2-model.dart';
import 'package:graficas/models/symbols-model.dart';
import 'package:graficas/models/ticker.model.dart';
import 'package:graficas/models/trades-model.dart';
import 'package:graficas/widgets/chart-widget.dart';
enum ServerStatus {
  Offline,
  Online,
  Connecting,
}
class VariablesService with ChangeNotifier {
  
  List<MyRow> graph = List();
  List<MyRow> get graphData => this.graph;
  set setGraphData(List<MyRow> value) {
    this.graph = value;
    notifyListeners();
  }

  Map<String, SymbolsModel> symbols;
  Map<String, SymbolsModel> get getSymbols => this.symbols;
  set setSymbols(Map<String, SymbolsModel> value) {
    this.symbols = value;
    notifyListeners();
  }
  
  ServerStatus serverStatus = ServerStatus.Connecting;
  //ServerStatus get serverStatus => this._serverStatus;

  String dropdownValue = 'BTC-USD';
  String get getDropdownValue => this.dropdownValue;
  set setDropdownValue(String value) {
    this.dropdownValue = value;
    notifyListeners();
  }

  TickerModel ticker;
  TickerModel get getTicker => this.ticker;
  double price24H = 1.0;
  double lastTradePrice = 1.0;

  double _priceBTC = 0.0;
  double get btc => this._priceBTC;

  int l2BidsCount = 0;
  List<L2Model> _l2Bids = [
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
  ];
  List<L2Model> get l2Bids => this._l2Bids;

  int l2AsksCount = 0;
  List<L2Model> _l2Asks = [
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
    L2Model(num: 0, px: 0.0, qty: 0.0),
  ];
  List<L2Model> get l2Asks => this._l2Asks;

  int tradesCount = 0;
  List<TradesModel> _trades = List(18);
  List<TradesModel> get trades => this._trades;

}

final globals = VariablesService();