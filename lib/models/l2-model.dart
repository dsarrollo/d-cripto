import 'dart:convert';

L2Model l2ModelFromJson(String str) => L2Model.fromJson(json.decode(str));

String l2ModelToJson(L2Model data) => json.encode(data.toJson());

class L2Model {
    L2Model({
        this.num,
        this.px,
        this.qty,
    });

    int num;
    double px;
    double qty;

    factory L2Model.fromJson(Map<String, dynamic> json) => L2Model(
        num: json["num"],
        px: json["px"].toDouble(),
        qty: json["qty"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "num": num,
        "px": px,
        "qty": qty,
    };
}
