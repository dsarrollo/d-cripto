import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graficas/services/http-services.dart';
import 'package:graficas/services/sockect-services.dart';
import 'package:graficas/services/variables-services.dart';
import 'package:graficas/widgets/chart-widget.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
// import 'package:intl/date_symbol_data_local.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class HomePage extends StatefulWidget {
  static String routerName = 'home';
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _showList = 1;

  @override
  void initState() {
    super.initState();
    httpService.getPrices();
    httpService.getSymbol();
    httpService.getTicker();
  }

  @override
  Widget build(BuildContext context) {
    final SocketService socketService = Provider.of<SocketService>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'D-Cripto',
          style: TextStyle(
            color: Colors.white54,
          ),
        ),
        backgroundColor: Color.fromRGBO(0, 0, 0, 0.8),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: (globals.serverStatus == ServerStatus.Online)
                ? Icon(Icons.check_circle, color: Colors.green)
                : Icon(Icons.offline_bolt, color: Colors.red),
          )
        ],
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          _backgroundApp(),
          ListView(
            children: [
              _dataOne(socketService),
              _graph(socketService),
              _dataTwo(socketService),
            ],
          ),
        ],
      ),
    );
  }

  Widget _backgroundApp() {
    final content1 = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset(0.0, 0.2),
          end: FractionalOffset(0.0, 1.0),
          colors: [
            Color.fromRGBO(14, 18, 27, 1.0),
            Colors.black,
          ],
        ),
      ),
    );
    return Stack(
      children: [
        content1,
      ],
    );
  }

  Widget _graph(SocketService socketService) {
    List<charts.Series<MyRow, DateTime>> _data = [
      new charts.Series<MyRow, DateTime>(
        id: 'Prices',
        domainFn: (MyRow row, _) => row.timeStamp,
        measureFn: (MyRow row, _) => row.price,
        data: globals.graphData,
      ),
    ];
    return Container(
      height: 200.0,
      width: double.infinity,
      child: charts.TimeSeriesChart(
        _data,
        animate: true,
        primaryMeasureAxis: new charts.NumericAxisSpec(
          tickProviderSpec:
              new charts.BasicNumericTickProviderSpec(zeroBound: false),
        ),
      ),
    );
  }

  Widget _dataOne(SocketService socketService) {
    return Card(
      elevation: 10,
      color: Color.fromRGBO(255, 255, 255, 0.1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          DropdownButton<String>(
            items: (globals.symbols != null)
                ? globals.symbols
                    .map((key, value) {
                      return MapEntry(
                          key,
                          DropdownMenuItem<String>(
                            value:
                                '${value.baseCurrency}-${value.counterCurrency}',
                            child: Row(
                              children: [
                                /* FutureBuilder(
                                  future: _buildImage(
                                      'assets/img/${value.baseCurrency}.png'),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<Image> snapshot) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.done) {
                                      return snapshot.data;
                                    } else {
                                      return Image.asset(
                                          'assets/img/exchange.png');
                                    }
                                  },
                                ), */
                                Image.asset(
                                  'assets/img/exchange.png',
                                  width: 20.0,
                                ),
                                /* Image(
                                  image: AssetImage('assets/img/exchange.png', ),
                                ), */
                                SizedBox(width: 5.0),
                                Text(
                                    '${value.baseCurrency}-${value.counterCurrency}',
                                    style: TextStyle(
                                      color: Colors.green,
                                    )),
                              ],
                            ),
                          ));
                    })
                    .values
                    .toList()
                : [
                    DropdownMenuItem<String>(
                      child: Text('BTC-USD',
                          style: TextStyle(
                            color: Colors.green,
                          )),
                      value: 'BTC-USD',
                    ),
                  ],
            onChanged: (String value) {
              socketService.unsubscribeTicker();
              socketService.unsubscribePrices();
              socketService.unsubscribeL2();
              globals.setDropdownValue = value;
              httpService.getTicker();
              httpService.getPrices();
              socketService.subscribeTicker();
              socketService.subscribePrices();
              socketService.subscribeL2();
            },
            value: globals.dropdownValue,
          ),
          Text(
              '${globals.ticker?.lastTradePrice} ${globals.dropdownValue.split("-")[1]}',
              style: TextStyle(
                color: Colors.green,
              )),
          /* Text(
            '${((socketService.ticker?.lastTradePrice ?? 1.0 / socketService.ticker?.price24H ?? 1.0) * 100) - 100} %',
            style: TextStyle(
              color: Colors.green,
            )), */
        ],
      ),
    );
  }

  Widget _dataTwo(SocketService socketService) {
    double valueSumBids = 0;
    double valueSumAsks = 0;
    return Card(
      elevation: 10,
      color: Color.fromRGBO(255, 255, 255, 0.1),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              RaisedButton(
                color: (_showList == 2)
                    ? Color.fromRGBO(14, 18, 27, 1.0)
                    : Color.fromRGBO(255, 255, 255, 0.8),
                onPressed: () {
                  setState(() {
                    _showList = 1;
                  });
                },
                child: Text('Libro de órdenes',
                    style: (_showList == 2)
                        ? TextStyle(fontSize: 14, color: Colors.white)
                        : TextStyle(fontSize: 14)),
              ),
              RaisedButton(
                color: (_showList == 1)
                    ? Color.fromRGBO(14, 18, 27, 1.0)
                    : Color.fromRGBO(255, 255, 255, 0.8),
                onPressed: () {
                  setState(() {
                    _showList = 2;
                  });
                },
                child: Text('Operaciones recientes',
                    style: (_showList == 1)
                        ? TextStyle(fontSize: 14, color: Colors.white)
                        : TextStyle(fontSize: 14)),
              ),
            ],
          ),
          SizedBox(height: 15.0),
          Visibility(
            visible: _showList == 1,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              height: MediaQuery.of(context).size.height * 0.45,
              child: Column(
                children: [
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: 8,
                    itemBuilder: (c, index) {
                      double sumTotal = globals.l2Asks.fold(
                          0, (previous, current) => previous + current.qty);
                      valueSumAsks = (index == 0)
                          ? sumTotal
                          : valueSumAsks - globals.l2Asks[index].qty;
                      return Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('${globals.l2Asks[index].px}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.red,
                                  )),
                              Text(
                                  '${globals.l2Asks[index].qty.toStringAsFixed(3)}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                              Text('${valueSumAsks.toStringAsFixed(3)}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                            ],
                          )
                        ],
                      );
                    },
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                        '${((globals.l2Asks[7].px + globals.l2Bids[0].px) / 2).toStringAsFixed(2)}',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                        )),
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: 8,
                    itemBuilder: (c, index) {
                      valueSumBids = (index == 0)
                          ? globals.l2Bids[index].qty
                          : valueSumBids + globals.l2Bids[index].qty;
                      return Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('${globals.l2Bids[index].px}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.green,
                                  )),
                              Text(
                                  '${globals.l2Bids[index].qty.toStringAsFixed(3)}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                              Text('${valueSumBids.toStringAsFixed(3)}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                            ],
                          )
                        ],
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: _showList == 2,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              height: MediaQuery.of(context).size.height * 0.45,
              child: ListView.builder(
                itemCount: 16,
                itemBuilder: (c, index) {
                  return globals.trades[index] != null
                      ? Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                  '${DateFormat.Hms().format(globals.trades[index].timestamp)}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                              Text(
                                  '${globals.trades[index].qty.toStringAsFixed(8)}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                              Text('${globals.trades[index].price}',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: (globals.trades[index] != null &&
                                            globals.trades[index].side ==
                                                'sell')
                                        ? Colors.green
                                        : Colors.red,
                                  )),
                            ],
                          ),
                        )
                      : Container();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Image> _buildImage(path) async {
    return rootBundle.load(path).then((value) {
      return Image.asset(
        path,
        height: 20.0,
      );
    }).catchError((_) {
      print('183 uqwueuqweuuqw');
      return Image.asset(
        'assets/img/no-image.jpg',
        height: 20.0,
      );
    });
  }
}
