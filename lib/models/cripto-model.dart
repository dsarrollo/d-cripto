import 'dart:convert';

import 'package:flutter/material.dart';

CriptosModel criptosModelFromJson(String str) =>
    CriptosModel.fromJson(json.decode(str));

String criptosModelToJson(CriptosModel data) => json.encode(data.toJson());

class CriptosModel {
  CriptosModel({
    this.criptos,
  });

  List<Cripto> criptos;

  factory CriptosModel.fromJson(Map<String, dynamic> json) => CriptosModel(
        criptos:
            List<Cripto>.from(json["criptos"].map((x) => Cripto.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Criptos": List<dynamic>.from(criptos.map((x) => x.toJson())),
      };
}

class Cripto {
  Cripto({
    this.code,
    this.color,
    this.name,
    this.price,
  });

  String code;
  Color color;
  String name;
  List<double> price;

  factory Cripto.fromJson(Map<String, dynamic> json) => Cripto(
        code: json["code"],
        name: json["name"],
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "price": price,
      };
}
