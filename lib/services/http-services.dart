import 'package:graficas/models/symbols-model.dart';
import 'package:graficas/models/ticker.model.dart';
import 'package:graficas/services/variables-services.dart';
import 'package:graficas/widgets/chart-widget.dart';
import 'package:http/http.dart' as http;
// import 'package:intl/intl.dart';
import 'package:graficas/models/prices-model.dart';

class HttpAPIService {
  // HttpAPIService() {}

  void getPrices() async {
    final url = Uri.https(
      'api.blockchain.com',
      'nabu-gateway/markets/exchange/prices',
      {
        'symbol': globals.dropdownValue,
        'start':
            '${DateTime.now().subtract(Duration(hours: 5)).millisecondsSinceEpoch}',
        'end': '${DateTime.now().millisecondsSinceEpoch}',
        'granularity': '300',
      },
    );
    final resp = await http.get(url);
    final dataResp = pricesModelFromJson(resp.body).prices;
    globals.setGraphData = List();
    for (var i = 0; i < dataResp.length; i++) {
      DateTime datePrice =
          DateTime.fromMicrosecondsSinceEpoch(dataResp[i][0].toInt());
      //String formattedTime = DateFormat.Hm().format(datePrice);
      globals.graphData.add(MyRow(datePrice, dataResp[i][2]));
    }
  }

  /* void getTrades() async {
    final url = Uri.https(
      'api.blockchain.com',
      'v3/exchange/trades/',
      {
        'symbol': socketService.dropdownValue,
        'from':
            '${DateTime.now().subtract(Duration(minutes: 5)).millisecondsSinceEpoch}',
        'to': '${DateTime.now().millisecondsSinceEpoch}',
        'limit': '18',
      },
    );
    final resp = await http.get(url, headers: {
      'X-API-Token': '7f7a0c84-b059-4c0a-9488-06525664177c',
      'accept': 'application/json'
    });
    final dataResp = tradesModelFromJson(resp.body);
  } */
  void getSymbol() async {
    final url = Uri.https(
      'api.blockchain.com',
      'v3/exchange/symbols',
    );
    final resp = await http.get(url, headers: {
      'X-API-Token': '7f7a0c84-b059-4c0a-9488-06525664177c',
      'accept': 'application/json'
    });
    final dataResp = symbolsModelFromJson(resp.body);
    globals.setSymbols = dataResp;
  }

  void getTicker() async {
    final resp = await http.get('https://api.blockchain.com/v3/exchange/tickers/${globals.dropdownValue}');
    final dataResp = tickerModelFromJson(resp.body);
    globals.ticker = dataResp;
  }

  // https://api.blockchain.com/mercury-gateway/v1/trades/1
  // https://api.blockchain.com/mercury-gateway/v1/instruments
}

final httpService = HttpAPIService();
