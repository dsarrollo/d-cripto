// To parse this JSON data, do
//
//     final tradesModel = tradesModelFromJson(jsonString);

import 'dart:convert';

TradesModel tradesModelFromJson(String str) =>
    TradesModel.fromJson(json.decode(str));

String tradesModelToJson(TradesModel data) => json.encode(data.toJson());

class TradesModel {
  TradesModel({
    this.tradeId,
    this.price,
    this.qty,
    this.side,
    this.timestamp,
  });

  String tradeId;
  double price;
  double qty;
  String side;
  DateTime timestamp;

  factory TradesModel.fromJson(Map<String, dynamic> json) => TradesModel(
        tradeId: json["trade_id"],
        price: json["price"].toDouble(),
        qty: json["qty"].toDouble(),
        side: json["side"],
        timestamp: DateTime.parse(json["timestamp"]),
      );

  Map<String, dynamic> toJson() => {
        "trade_id": tradeId,
        "price": price,
        "qty": qty,
        "side": side,
        "timestamp": timestamp.toIso8601String(),
      };
}
