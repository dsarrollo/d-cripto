import 'dart:convert';

TickerModel tickerModelFromJson(String str) =>
    TickerModel.fromJson(json.decode(str));

String tickerModelToJson(TickerModel data) => json.encode(data.toJson());

class TickerModel {
  TickerModel({
    this.seqnum,
    this.event,
    this.channel,
    this.symbol,
    this.price24H,
    this.volume24H,
    this.lastTradePrice,
  });

  int seqnum;
  String event;
  String channel;
  String symbol;
  double price24H;
  double volume24H;
  double lastTradePrice;

  factory TickerModel.fromJson(Map<String, dynamic> json) => TickerModel(
        seqnum: json["seqnum"],
        event: json["event"],
        channel: json["channel"],
        symbol: json["symbol"],
        price24H: (json["price_24h"] == null) ? 0.0 : json["price_24h"],
        volume24H: (json["volume_24h"] == null) ? 0.0 : json["volume_24h"],
        lastTradePrice:
            (json["last_trade_price"] == null) ? 0.0 : json["last_trade_price"],
      );

  Map<String, dynamic> toJson() => {
        "seqnum": seqnum,
        "event": event,
        "channel": channel,
        "symbol": symbol,
        "price_24h": price24H,
        "volume_24h": volume24H,
        "last_trade_price": lastTradePrice,
      };
}
