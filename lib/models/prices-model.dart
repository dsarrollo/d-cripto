import 'dart:convert';

PricesModel pricesModelFromJson(String str) =>
    PricesModel.fromJson(json.decode(str));

String pricesModelToJson(PricesModel data) => json.encode(data.toJson());

class PricesModel {
  PricesModel({
    this.prices,
  });

  List<List<double>> prices;

  factory PricesModel.fromJson(Map<String, dynamic> json) => PricesModel(
        prices: List<List<double>>.from(json["prices"]
            .map((x) => List<double>.from(x.map((x) => x.toDouble())))),
      );

  Map<String, dynamic> toJson() => {
        "prices": List<dynamic>.from(
            prices.map((x) => List<dynamic>.from(x.map((x) => x)))),
      };
}
// modelo del websocket
PriceWebsocketModel priceWebsocketModelFromJson(String str) =>
    PriceWebsocketModel.fromJson(json.decode(str));

String priceWebsocketModelToJson(PriceWebsocketModel data) =>
    json.encode(data.toJson());

class PriceWebsocketModel {
  PriceWebsocketModel({
    this.price,
  });

  List<double> price;

  factory PriceWebsocketModel.fromJson(Map<String, dynamic> json) =>
      PriceWebsocketModel(
        price: List<double>.from(json["price"].map((x) => x.toDouble())),
      );

  Map<String, dynamic> toJson() => {
        "price": List<dynamic>.from(price.map((x) => x)),
      };
}
